#! /usr/bin/env python
# -*- coding: utf-8 -*-
from socket import AF_INET, SOCK_STREAM, socket
from threading import Thread
import time

clients = {}
addresses = {}

HOST = 'localhost'
PORT = 8080
ADDRES = (HOST, PORT)
size_buffer = 1024

SERVER = socket(AF_INET, SOCK_STREAM)

SERVER.bind(ADDRES)


def accept_new_connections():
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s подключился." % client_address)
        # print("ID: " + str(client_address[1]))
        client.send(bytes("Приветствую, введите ваше имя!", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()


def handle_client(client):
    nick_name = client.recv(size_buffer).decode("utf8")
    welcome = 'Привет %s! Для выхода из чата введтие: !выход' % nick_name
    client.send(bytes(welcome, "utf8"))
    message = "%s присоеденился к чату! Нас уже - %s в чате!" % (nick_name, len(clients) + 1)
    transfer(bytes(message, "utf8"))
    clients[client] = nick_name

    while True:
        message = client.recv(size_buffer)
        if message != bytes("!выход", "utf8"):
            transfer(message, nick_name + ": ")
            print(time.strftime("%H:%M:%S - ") + str(nick_name) + " (ID:" + str(client.getpeername()[1]) + "): " + str(
                message, 'utf-8'))
            # print("ID - " + str(client.getpeername()[1]))
        else:
            client.send(bytes("!выход", "utf8"))
            client.close()
            del clients[client]
            transfer(bytes("%s покинул чат." % nick_name, "utf8"))
            print("Пользователь: %s покинул чат" % nick_name)
            break


def transfer(msg, prefix=""):
    for sock in clients:
        sock.send(bytes(time.strftime("%H:%M:%S - "), 'utf-8') + bytes(prefix, "utf8") + msg)


if __name__ == "__main__":
    SERVER.listen(5)
    print("Сервер запущен: " + str(HOST) +":"+ str(PORT) + "\nОжидание подключения клиентов...")
    ACCEPT_THREAD = Thread(target=accept_new_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    SERVER.close()
