#! /usr/bin/env python
# -*- coding: utf-8 -*-
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import tkinter


def obtain():
    while True:
        try:
            message = client_socket.recv(BUFSIZ).decode("utf8")
            list_of_messages.insert(tkinter.END, message)
        except OSError:
            break


def send_a_message(event=None):
    message = my_message.get()
    my_message.set("")
    client_socket.send(bytes(message, "utf8"))
    if message == "!выход":
        client_socket.close()
        main_Tk.quit()


def close_a_window(event=None):
    my_message.set("!выход")
    send_a_message()


main_Tk = tkinter.Tk()
main_Tk.title("Врублевский Виталий - чат")
main_Tk.configure(background='gray80')
main_Tk.columnconfigure(0, weight=1)

messages_frame = tkinter.Frame(main_Tk)
my_message = tkinter.StringVar(value="Ваш ник")
scrollbar = tkinter.Scrollbar(messages_frame)
list_of_messages = tkinter.Listbox(messages_frame, height=20, width=100, yscrollcommand=scrollbar.set,
                                   selectbackground='red')
list_of_messages.configure(background='gray80')
scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
list_of_messages.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
list_of_messages.pack()
messages_frame.pack()

field_to_entry = tkinter.Entry(main_Tk, textvariable=my_message, width=100)
field_to_entry.bind("<Return>", send_a_message)
field_to_entry.configure(background='SteelBlue3')
field_to_entry.pack()

label_exit = tkinter.Label(text="Для выхода введите: !выход", fg="#eee", bg="#333")
label_exit.pack()

send_button = tkinter.Button(main_Tk, text="Отправить или Enter", command=send_a_message)
send_button.configure(background='SteelBlue3')
send_button.pack()

main_Tk.protocol("WM_DELETE_WINDOW", close_a_window)

HOST = input('Введите ip сервера (или оставьте пустым): ')
PORT = input('Введите порт (или оставьте пустым): ')
if not PORT:
    PORT = 8080
else:
    PORT = int(PORT)

if not HOST:
    HOST = '127.0.0.1'

BUFSIZ = 1024
ADDR = (HOST, PORT)

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(ADDR)

obtain_thread = Thread(target=obtain)
obtain_thread.start()
tkinter.mainloop()
